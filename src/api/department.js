import request from '@/utils/request'
// 获取部门数据
export const getDepartments = () => {
  return request.get('/company/department')
}
// 获取部门负责人
export const getDepartmentSimpleService = () => {
  return request.get('/sys/user/simple')
}
// 添加本门
export const addDepartment = (data) => {
  return request.post('/company/department', data)
}
// 编辑部门
export const updateDepartment = (data) => {
  return request.put(`/company/department/${data.id}`, data)
}
// 获取部门详情
export const getDepartmentDetail = (id) => {
  return request.get(`/company/department/${id}`)
}
// 删除部门
export const delDepartment = (id) => {
  return request.delete(`/company/department/${id}`)
}
