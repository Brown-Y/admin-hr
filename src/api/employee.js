import request from '@/utils/request'
// 获取员工基本信息
export const getEmployeeList = (id) => {
  return request.get(`/sys/user/${id}`)
}
// 获取员工列表
export const getEmployeeSimpleList = ({ page, pagesize, keyword, departmentId }) => {
  return request.get('/sys/user', { params: { page, pagesize, keyword, departmentId }})
}
// 添加员工
export const addEmployee = (data) => {
  return request.post('/sys/user', data)
}
// 修改员工信息
export const updateEmployee = (data) => {
  return request.put(`/sys/user/${data.id}`, data)
}
// 删除员工
export const delEmployee = (id) => {
  return request.delete(`/sys/user/${id}`)
}
// 下载模版
export const importEmployee = () => {
  return request.get('/sys/user/import/template', { responseType: 'blob' })
}
// 导入excel formData格式
export const importEmployeeInfo = (data) => {
  return request.post('/sys/user/import', data)
}
// 导出excel 二进制文件流
export const exportEmployee = () => {
  return request.get('/sys/user/export', { responseType: 'blob' })
}
