import request from '@/utils/request'
// 登录获取token
export const loginService = (data) => {
  return request.post('/sys/login', data)
}
// 获取用户信息
export const getUserInfoService = () => {
  return request.get('/sys/profile')
}
// 修改密码
export const updatePasswordService = (data) => {
  return request.put('/sys/user/password', data)
}
