import request from '@/utils/request'
// 获取角色分页列表
export const getRoleList = ({ page, pagesize }) => {
  return request.get('/sys/role', { params: { page, pagesize }})
}
// 获取角色详情
export const getRoleDetail = (id) => {
  return request.get(`/sys/role/${id}`)
}
// 新增角色
export const addRole = (data) => {
  return request.post('/sys/role', data)
}
// 删除角色
export const deleteRole = (id) => {
  return request.delete(`/sys/role/${id}`)
}
//  修改角色
export const updateRole = (data) => {
  return request.put(`/sys/role/${data.id}`, data)
}
// 分配权限
export const assignPerm = (data) => {
  return request.put(`/sys/role/assignPrem`, data)
}
