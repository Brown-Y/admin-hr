import axios from 'axios'
import store from '@/store'
import router from '@/router'
import { Message } from 'element-ui'
const instance = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: 100000
})
// 添加请求拦截器
instance.interceptors.request.use(function(config) {
  // 在发送请求之前做些什么
  if (store.getters.token) {
    config.headers.Authorization = `Bearer ${store.getters.token}`
  }
  return config
}, function(error) {
  // 对请求错误做些什么
  return Promise.reject(error)
})

// 添加响应拦截器
instance.interceptors.response.use(function(response) {
  // 2xx 范围内的状态码都会触发该函数。
  // 对响应数据做点什么
  if (response.data instanceof Blob) return response.data
  if (response.data.success) {
    return response.data
  } else {
    return Promise.rejecet(new Error(response.data.error?.message))
  }
}, async function(error) {
  // 超出 2xx 范围的状态码都会触发该函数。
  // 对响应错误做点什么
  if (error.response.status === 401) {
    store.dispatch('user.logout')
    router.push('/login')
    this.$message.error('登录已过期，请重新登录')
  }
  Message.error(error.message)
  return Promise.reject(error)
})
export default instance
