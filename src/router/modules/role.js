import layout from '@/layout'

export default {
  path: '/role',
  component: layout,
  children: [{
    path: '',
    name: 'role',
    component: () => import('@/views/role/index'),
    meta: { title: '角色', icon: 'setting' }
  }]
}
