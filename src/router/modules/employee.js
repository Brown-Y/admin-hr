import layout from '@/layout'
export default {
  path: '/employee',
  component: layout,
  children: [{
    path: '',
    component: () => import('@/views/employee/index'),
    name: 'employee',
    meta: {
      title: '员工管理',
      icon: 'people'
    }
  },
  {
    path: '/employee/detail/:id?',
    component: () => import('@/views/employee/employeeDetail'),
    hidden: true,
    meta: {
      title: '员工详情'
    }
  }]
}
