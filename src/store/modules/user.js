// cookie存储持久化数据
import { getToken, setToken, removeToken } from '@/utils/auth'
import { loginService, getUserInfoService } from '@/api/user'
const state = {
  token: getToken(),
  // 用户信息
  userInfo: {}
}
const mutations = {
  // 存储token
  setToken(state, data) {
    state.token = data
    setToken(data)
  },

  // 存储用户信息
  setUserInfo(state, data) {
    state.userInfo = data
  },
  // 清除信息
  removeToken(state) {
    state.token = ''
    removeToken()
  }
}
const actions = {
  // 登录
  async login(context, data) {
    const res = await loginService(data)
    // console.log(res)
    context.commit('setToken', res.data)
  },
  // 获取用户信息
  async getUserInfo(context) {
    const { data } = await getUserInfoService()
    context.commit('setUserInfo', data)
  },
  // 退出登录
  logout(context) {
    context.commit('removeToken')
    context.commit('setUserInfo', {})
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
