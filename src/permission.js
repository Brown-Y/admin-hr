import store from '@/store'
import router from '@/router'
import nprogress from 'nprogress'
import 'nprogress/nprogress.css'
// 前置守卫
// 白名单
const whiteList = ['/login', '/404']
router.beforeEach(async(to, from, next) => {
  // 开启进度条
  nprogress.start()
  // 判断是否需要登录
  if (store.getters.token) {
    if (to.path === '/login') {
      next('/')
      nprogress.done()
    } else {
      if (!store.getters.userId) {
        await store.dispatch('user/getUserInfo')
      }
      next()
    }
  } else {
    if (whiteList.includes(to.path)) {
      next()
    } else {
      next('/login')
      nprogress.done()
    }
  }
})
router.afterEach(() => {
  nprogress.done()
})
